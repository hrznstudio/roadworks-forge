package com.hrznstudio.roadworks.forge.wrapper;

import com.hrznstudio.roadworks.layer.api.common.block.entity.BlockEntity;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.EnumFacing;
import net.minecraftforge.common.capabilities.Capability;
import net.minecraftforge.common.capabilities.OptionalCapabilityInstance;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class TileEntityWrapper extends TileEntity {
    private BlockEntity blockEntity;

    public TileEntityWrapper(BlockEntity blockEntity) {
        super(null);
        this.blockEntity = blockEntity;
    }

    public BlockEntity getBlockEntity() {
        return blockEntity;
    }

    @Override
    public void read(NBTTagCompound p_145839_1_) {
        super.read(p_145839_1_);
    }

    @Override
    public NBTTagCompound write(NBTTagCompound p_189515_1_) {
        return super.write(p_189515_1_);
    }

    @Nonnull
    @Override
    public <T> OptionalCapabilityInstance<T> getCapability(@Nonnull Capability<T> cap, @Nullable EnumFacing side) {
        return super.getCapability(cap, side);
    }
}
