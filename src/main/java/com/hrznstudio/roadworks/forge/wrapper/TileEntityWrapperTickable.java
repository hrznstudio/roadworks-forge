package com.hrznstudio.roadworks.forge.wrapper;

import com.hrznstudio.roadworks.layer.api.common.block.entity.BlockEntity;
import net.minecraft.util.ITickable;

public class TileEntityWrapperTickable extends TileEntityWrapper implements ITickable {
    public TileEntityWrapperTickable(BlockEntity blockEntity) {
        super(blockEntity);
    }

    @Override
    public void tick() {
        getBlockEntity().onTick();
    }
}
