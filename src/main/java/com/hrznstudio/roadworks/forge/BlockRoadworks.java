package com.hrznstudio.roadworks.forge;

import com.hrznstudio.roadworks.forge.util.Util;
import com.hrznstudio.roadworks.layer.api.common.Activation;
import com.hrznstudio.roadworks.layer.api.common.block.Block;
import com.hrznstudio.roadworks.layer.api.common.block.BlockEntityProvider;
import com.hrznstudio.roadworks.layer.api.common.block.IFluidHolder;
import com.hrznstudio.roadworks.layer.api.common.math.Vec3;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;
import net.minecraft.block.IBucketPickupHandler;
import net.minecraft.block.ILiquidContainer;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.fluid.Fluid;
import net.minecraft.fluid.IFluidState;
import net.minecraft.init.Fluids;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.Vec3i;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.World;

import javax.annotation.Nullable;

public class BlockRoadworks extends net.minecraft.block.Block implements ILiquidContainer, IBucketPickupHandler {
    private Block block;

    public BlockRoadworks(Builder p_i48440_1_) {
        super(p_i48440_1_);
    }

    @Override
    public EnumBlockRenderType getRenderType(IBlockState state) {
        return Util.convert(block.getRenderType(Util.convert(state)));
    }

    @Override
    public boolean canRenderInLayer(IBlockState state, BlockRenderLayer layer) {
        return block.canRenderInLayer(
                Util.convert(layer),
                Util.convert(state)
        );
    }

    @Override
    public boolean onBlockActivated(IBlockState state, World world, BlockPos pos, EntityPlayer player, EnumHand hand, EnumFacing side, float x, float y, float z) {
        return block.onBlockUsed(
                Util.convert(world),
                Util.convert(player),
                Util.convert(pos),
                Util.convert(state),
                Util.convert(hand),
                Util.convert(side),
                (Vec3) new Vec3i(x, y, z)
        ) == Activation.SUCCESS;
    }

    @Override
    public boolean canContainFluid(IBlockReader iBlockReader, BlockPos blockPos, IBlockState iBlockState, Fluid fluid) {
        if (block instanceof IFluidHolder)
            return ((IFluidHolder) block).canContainFluid(
                    Util.convert(iBlockReader),
                    Util.convert(blockPos),
                    Util.convert(iBlockState),
                    Util.convert(fluid)
            );
        return false;
    }

    @Override
    public boolean receiveFluid(IWorld iWorld, BlockPos blockPos, IBlockState iBlockState, IFluidState iFluidState) {
        if (block instanceof IFluidHolder)
            return ((IFluidHolder) block).setFluidState(
                    Util.convert(iWorld),
                    Util.convert(blockPos),
                    Util.convert(iBlockState),
                    Util.convert(iFluidState)
            );
        return false;
    }

    @Override
    public Fluid pickupFluid(IWorld iWorld, BlockPos blockPos, IBlockState iBlockState) {
        if (block instanceof IFluidHolder)
            return ((IFluidHolder) block).emptyFluidState(
                    Util.convert(iWorld),
                    Util.convert(blockPos),
                    Util.convert(iBlockState))
                    .map(Util::convert)
                    .orElse(Fluids.EMPTY);
        return Fluids.EMPTY;
    }

    @Nullable
    @Override
    public TileEntity createTileEntity(IBlockState state, IBlockReader world) {
        if (block instanceof BlockEntityProvider) {
            BlockState blockState = Util.convert(state);
            return Util.wrapper(((BlockEntityProvider) block).isBlockEntityTickable(blockState), ((BlockEntityProvider) block).createBlockEntity(blockState));
        }
        return null;
    }

    @Override
    public boolean hasTileEntity(IBlockState state) {
        if (block instanceof BlockEntityProvider)
            return ((BlockEntityProvider) block).hasBlockEntity(Util.convert(state));
        return false;
    }
}