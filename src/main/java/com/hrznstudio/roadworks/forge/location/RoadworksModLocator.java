package com.hrznstudio.roadworks.forge.location;

import com.google.common.collect.Lists;
import com.hrznstudio.roadworks.forge.RoadworksPaths;
import net.minecraftforge.fml.loading.StringUtils;
import net.minecraftforge.fml.loading.moddiscovery.IModLocator;
import net.minecraftforge.fml.loading.moddiscovery.ModFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.FileSystem;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.*;
import java.util.function.Consumer;
import java.util.jar.JarFile;
import java.util.jar.Manifest;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipError;

import static cpw.mods.modlauncher.api.LamdbaExceptionUtils.uncheck;
import static net.minecraftforge.fml.Logging.SCAN;

public class RoadworksModLocator implements IModLocator {
    private static final String SUFFIX = ".roadworks.jar";
    private static final Logger LOGGER = LogManager.getLogger();
    private final List<Path> modFolders;
    private final Map<ModFile, FileSystem> modJars;

    public RoadworksModLocator() {
        this(RoadworksPaths.PLUGINSDIR.get());
    }

    private RoadworksModLocator(Path... modFolders) {
        this.modFolders = Lists.newArrayList(modFolders);
        this.modJars = new HashMap<>();
    }

    @Override
    public List<ModFile> scanMods() {
        return modFolders.stream().flatMap(modFolder -> uncheck(() -> Files.list(modFolder)))
                .filter(p -> StringUtils.toLowerCase(p.getFileName().toString()).endsWith(SUFFIX))
                .sorted(Comparator.comparing(path -> StringUtils.toLowerCase(path.getFileName().toString())))
                .map(p -> new ModFile(p, this))
                .peek(f -> modJars.compute(f, (mf, fs) -> createFileSystem(mf)))
                .collect(Collectors.toList());
    }

    private FileSystem createFileSystem(ModFile modFile) {
        try {
            return FileSystems.newFileSystem(modFile.getFilePath(), modFile.getClass().getClassLoader());
        } catch (ZipError | IOException e) {
            LOGGER.debug(SCAN, "Ignoring invalid JAR file {}", modFile.getFilePath());
            return null;
        }
    }

    @Override
    public String name() {
        return "roadworks";
    }

    @Override
    public Path findPath(ModFile modFile, String... path) {
        if (path.length < 1) {
            throw new IllegalArgumentException("Missing path");
        }
        return modJars.get(modFile).getPath(path[0], Arrays.copyOfRange(path, 1, path.length));
    }

    @Override
    public void scanFile(ModFile file, Consumer<Path> pathConsumer) {
        LOGGER.debug(SCAN, "Scan started: {}", file);
        FileSystem fs = modJars.get(file);
        fs.getRootDirectories().forEach(path -> {
            try (Stream<Path> files = Files.find(path, Integer.MAX_VALUE, (p, a) -> p.getNameCount() > 0 && p.getFileName().toString().endsWith(".class"))) {
                files.forEach(pathConsumer);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
        LOGGER.debug(SCAN, "Scan finished: {}", file);
    }

    @Override
    public Optional<Manifest> findManifest(Path file) {
        try (JarFile jf = new JarFile(file.toFile())) {
            return Optional.ofNullable(jf.getManifest());
        } catch (IOException e) {
            return Optional.empty();
        }
    }
}