package com.hrznstudio.roadworks.forge.server;

import com.hrznstudio.roadworks.layer.api.common.world.Difficulty;
import com.hrznstudio.roadworks.layer.api.server.GameType;
import com.hrznstudio.roadworks.layer.api.server.ServerProperties;
import net.minecraft.server.MinecraftServer;

import javax.annotation.Nonnull;

public class ServerPropertiesImpl implements ServerProperties {
    private MinecraftServer server;

    public ServerPropertiesImpl(@Nonnull MinecraftServer server) {
        this.server = server;
    }

    @Override
    public boolean isWhitelistEnabled() {
        return server.isWhitelistEnabled();
    }

    @Override
    public long getSeed() {
        return -1;
    }

    @Override
    public String getMOTD() {
        return server.getMOTD();
    }

    @Override
    public int getMaxPlayers() {
        return server.getMaxPlayers();
    }

    @Override
    public boolean isPVPEnabled() {
        return server.isPVPEnabled();
    }

    @Override
    public boolean areCommandBlocksEnabled() {
        return server.isCommandBlockEnabled();
    }

    @Override
    public Difficulty getDifficulty() {
        return Difficulty.from(server.getDifficulty().getId());
    }

    @Override
    public GameType getGameType() {
        return GameType.getByID(server.getGameType().getID());
    }
}