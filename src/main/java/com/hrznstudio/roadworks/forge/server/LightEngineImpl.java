package com.hrznstudio.roadworks.forge.server;

import com.hrznstudio.roadworks.forge.util.Util;
import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.world.light.LightEngine;
import net.minecraft.world.World;

public class LightEngineImpl implements LightEngine {
    private final World world;

    public LightEngineImpl(World world) {
        this.world = world;
    }

    @Override
    public int getLightAt(Position position) {
        return world.getLight(Util.convert(position));
    }
}
