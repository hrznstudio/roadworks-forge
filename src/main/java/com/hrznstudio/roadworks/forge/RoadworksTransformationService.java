package com.hrznstudio.roadworks.forge;

import cpw.mods.modlauncher.api.IEnvironment;
import cpw.mods.modlauncher.api.ITransformationService;
import cpw.mods.modlauncher.api.ITransformer;
import cpw.mods.modlauncher.api.IncompatibleEnvironmentException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.annotation.Nonnull;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import static net.minecraftforge.fml.Logging.CORE;

public class RoadworksTransformationService implements ITransformationService {
    private static final Logger LOGGER = LogManager.getLogger();

    @Nonnull
    @Override
    public String name() {
        return "roadworks";
    }

    @Override
    public void initialize(IEnvironment environment) {
        LOGGER.debug(CORE, "Setting up roadworks directories");
        RoadworksPaths.setup(environment);
    }

    @Override
    public void onLoad(IEnvironment env, Set<String> otherServices) throws IncompatibleEnvironmentException {
        if (otherServices.stream().noneMatch(s -> s.equals("fml")))
            throw new IncompatibleEnvironmentException("Roadworks Forge requires forge to run");
    }

    @Nonnull
    @Override
    public List<ITransformer> transformers() {
        return Collections.emptyList();
    }
}