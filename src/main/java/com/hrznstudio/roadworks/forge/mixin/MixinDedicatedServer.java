package com.hrznstudio.roadworks.forge.mixin;

import com.hrznstudio.roadworks.forge.server.ServerPropertiesImpl;
import com.hrznstudio.roadworks.layer.api.server.ServerProperties;
import com.mojang.authlib.GameProfileRepository;
import com.mojang.authlib.minecraft.MinecraftSessionService;
import com.mojang.authlib.yggdrasil.YggdrasilAuthenticationService;
import com.mojang.datafixers.DataFixer;
import net.minecraft.command.Commands;
import net.minecraft.server.MinecraftServer;
import net.minecraft.server.dedicated.DedicatedServer;
import net.minecraft.server.management.PlayerProfileCache;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;

import javax.annotation.Nullable;
import java.io.File;
import java.net.Proxy;

@Mixin(DedicatedServer.class)
@Implements(@Interface(iface = com.hrznstudio.roadworks.layer.api.server.DedicatedServer.class, prefix = "rd$"))
public abstract class MixinDedicatedServer extends MinecraftServer {
    private final ServerProperties properties = new ServerPropertiesImpl(this);

    public MixinDedicatedServer(@Nullable File p_i49697_1_, Proxy p_i49697_2_, DataFixer p_i49697_3_, Commands p_i49697_4_, YggdrasilAuthenticationService p_i49697_5_, MinecraftSessionService p_i49697_6_, GameProfileRepository p_i49697_7_, PlayerProfileCache p_i49697_8_) {
        super(p_i49697_1_, p_i49697_2_, p_i49697_3_, p_i49697_4_, p_i49697_5_, p_i49697_6_, p_i49697_7_, p_i49697_8_);
    }

    public ServerProperties rd$getProperties() {
        return properties;
    }
}