package com.hrznstudio.roadworks.forge.mixin.scoreboard;

import com.hrznstudio.roadworks.layer.api.common.player.Player;
import com.hrznstudio.roadworks.layer.api.common.scoreboard.Objective;
import com.hrznstudio.roadworks.layer.api.common.scoreboard.Scoreboard;
import com.hrznstudio.roadworks.layer.api.common.scoreboard.Team;
import net.minecraft.scoreboard.ScorePlayerTeam;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Mixin(net.minecraft.scoreboard.Scoreboard.class)
@Implements(@Interface(iface = Scoreboard.class, prefix = "rd$"))
public abstract class MixinScoreboard {
    @Shadow
    public abstract Collection<ScorePlayerTeam> getTeams();

    public List<Objective<?>> rd$getObjectives() {
        return null;
    }

    public List<Team> rd$getTeams() {
        return getTeams().stream().map(Team.class::cast).collect(Collectors.toList());
    }

    public <V> Optional<V> rd$getValue(Player player, Objective<V> objective) {
        return Optional.empty();
    }

    public Optional<Team> rd$getTeam(Player player) {
        return Optional.empty();
    }
}