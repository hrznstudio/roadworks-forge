package com.hrznstudio.roadworks.forge.mixin.math;

import net.minecraft.util.math.Vec3i;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Vec3i.class)
@Implements(@Interface(iface = Vec3i.class, prefix = "rd$"))
public abstract class MixinVec3i {

    @Shadow
    public abstract int getX();

    @Shadow
    public abstract int getY();

    @Shadow
    public abstract int getZ();

    public int rd$getX() {
        return this.getX();
    }

    public int rd$getY() {
        return this.getY();
    }

    public int rd$getZ() {
        return this.getZ();
    }
}
