package com.hrznstudio.roadworks.forge.mixin.scoreboard;

import com.hrznstudio.roadworks.forge.util.Util;
import com.hrznstudio.roadworks.layer.api.common.scoreboard.Team;
import com.hrznstudio.roadworks.layer.api.common.text.Text;
import net.minecraft.util.text.ITextComponent;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import javax.annotation.Nullable;

@Mixin(net.minecraft.scoreboard.ScorePlayerTeam.class)
@Implements(@Interface(iface = Team.class, prefix = "rd$"))
public abstract class MixinTeam {
    @Shadow
    public abstract ITextComponent getSuffix();

    @Shadow
    public abstract void setSuffix(@Nullable ITextComponent p_207409_1_);

    @Shadow
    public abstract ITextComponent getPrefix();

    @Shadow
    public abstract void setPrefix(@Nullable ITextComponent p_207408_1_);

    public Team.Options rd$getOptions() {
        return null;
    }

    public Text rd$getPrefix() {
        return Util.convert(getPrefix());
    }

    public void rd$setPrefix(Text text) {
        setPrefix(Util.convert(text));
    }

    public Text rd$getSuffix() {
        return Util.convert(getSuffix());
    }

    public void rd$setSuffix(Text text) {
        setSuffix(Util.convert(text));
    }
}