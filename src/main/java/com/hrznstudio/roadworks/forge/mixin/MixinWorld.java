package com.hrznstudio.roadworks.forge.mixin;

import com.hrznstudio.roadworks.forge.server.LightEngineImpl;
import com.hrznstudio.roadworks.layer.api.common.server.Server;
import com.hrznstudio.roadworks.layer.api.common.world.World;
import com.hrznstudio.roadworks.layer.api.common.world.light.LightEngine;
import net.minecraft.server.MinecraftServer;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import javax.annotation.Nullable;

@Mixin(net.minecraft.world.World.class)
@Implements(@Interface(iface = World.class, prefix = "rd$"))
public abstract class MixinWorld {
    private final LightEngine engine = new LightEngineImpl((net.minecraft.world.World) (Object) this);

    @Shadow
    @Nullable
    public abstract MinecraftServer getServer();

    public Server rd$getServer() {
        //TODO: if null do smth
        return (Server) getServer();
    }

    public LightEngine rd$getLightEngine() {
        return engine;
    }
}
