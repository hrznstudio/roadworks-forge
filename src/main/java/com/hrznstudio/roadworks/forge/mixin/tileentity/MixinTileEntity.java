package com.hrznstudio.roadworks.forge.mixin.tileentity;

import com.hrznstudio.roadworks.forge.util.Util;
import com.hrznstudio.roadworks.layer.api.common.block.entity.BlockEntity;
import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.world.World;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import javax.annotation.Nullable;

@Mixin(TileEntity.class)
@Implements(@Interface(iface = BlockEntity.class, prefix = "rd$"))
public abstract class MixinTileEntity {
    @Shadow
    @Nullable
    public abstract net.minecraft.world.World getWorld();

    @Shadow
    public abstract BlockPos getPos();

    public Position rd$getPos() {
        return Util.convert(getPos());
    }

    public World rd$getWorld() {
        return Util.convert(getWorld());
    }

    public void rd$onTick() {
        if (this instanceof ITickable) {
            ((ITickable) this).tick();
        }
    }
}
