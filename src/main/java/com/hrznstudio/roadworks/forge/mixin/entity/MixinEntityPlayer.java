package com.hrznstudio.roadworks.forge.mixin.entity;

import com.hrznstudio.roadworks.forge.util.Util;
import com.hrznstudio.roadworks.layer.api.common.command.SourceType;
import com.hrznstudio.roadworks.layer.api.common.entity.Entity;
import com.hrznstudio.roadworks.layer.api.common.permission.Node;
import com.hrznstudio.roadworks.layer.api.common.player.Player;
import com.hrznstudio.roadworks.layer.api.common.player.Profile;
import com.hrznstudio.roadworks.layer.api.common.text.Text;
import com.mojang.authlib.GameProfile;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.text.ITextComponent;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(EntityPlayer.class)
@Implements(@Interface(iface = Player.class, prefix = "rd$"))
public abstract class MixinEntityPlayer {
    @Shadow
    public abstract GameProfile getGameProfile();

    @Shadow
    public abstract ITextComponent getDisplayName();

    @Shadow
    public abstract void sendMessage(ITextComponent component);

    public Entity rd$getEntityObject() {
        return Util.convert((net.minecraft.entity.Entity) (Object) this);
    }

    public Text rd$getDisplayName() {
        return Util.convert(getDisplayName());
    }

    public SourceType rd$getType() {
        return SourceType.PLAYER;
    }

    public boolean rd$hasPermission(Node node) {
        return false;
    }

    public Profile rd$getProfile() {
        return (Profile) getGameProfile();
    }

    public void rd$sendMessage(Text message) {
        sendMessage(Util.convert(message));
    }
}
