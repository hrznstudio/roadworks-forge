package com.hrznstudio.roadworks.forge.mixin;

import com.hrznstudio.roadworks.layer.api.common.player.Player;
import com.hrznstudio.roadworks.layer.api.common.scoreboard.Scoreboard;
import com.hrznstudio.roadworks.layer.api.common.server.Server;
import com.hrznstudio.roadworks.layer.api.common.world.World;
import net.minecraft.scoreboard.ServerScoreboard;
import net.minecraft.server.MinecraftServer;
import net.minecraft.world.WorldServer;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Mixin(MinecraftServer.class)
@Implements(@Interface(iface = Server.class, prefix = "rd$"))
public abstract class MixinMinecraftServer {
    @Shadow
    public WorldServer[] worlds;

    @Shadow
    private ServerScoreboard scoreboard;

    public List<World> rd$getLoadedWorlds() {
        return Arrays.stream(worlds).map(World.class::cast).collect(Collectors.toList());
    }

    public Optional<World> rd$getWorld(String world) {
        return Optional.empty();
    }

    public List<Player> rd$getOnlinePlayers() {
        return null;
    }

    public Scoreboard rd$getScoreboard() {
        return (Scoreboard) scoreboard;
    }
}