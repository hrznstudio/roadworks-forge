package com.hrznstudio.roadworks.forge.mixin;

import com.hrznstudio.roadworks.forge.util.Util;
import com.hrznstudio.roadworks.layer.api.common.block.BlockReader;
import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;
import com.hrznstudio.roadworks.layer.api.common.state.FluidState;
import net.minecraft.block.state.IBlockState;
import net.minecraft.fluid.IFluidState;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(net.minecraft.world.IBlockReader.class)
@Implements(@Interface(iface = BlockReader.class, prefix = "rd$"))
public abstract class MixinBlockReader {

    @Shadow
    public abstract IFluidState getFluidState(BlockPos blockPos);

    @Shadow
    public abstract IBlockState getBlockState(BlockPos blockPos);

    public BlockState rd$getBlockState(Position pos) {
        return Util.convert(getBlockState(Util.convert(pos)));
    }

    public FluidState rd$getFluidState(Position pos) {
        return Util.convert(getFluidState(Util.convert(pos)));
    }
}