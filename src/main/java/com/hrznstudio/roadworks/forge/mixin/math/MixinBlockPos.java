package com.hrznstudio.roadworks.forge.mixin.math;

import com.hrznstudio.roadworks.layer.api.common.math.MutablePosition;
import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.world.Facing;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(BlockPos.class)
@Implements(@Interface(iface = Position.class, prefix = "rd$"))
public abstract class MixinBlockPos {

    @Shadow
    public abstract BlockPos offset(EnumFacing enumFacing);

    @Shadow
    public abstract BlockPos offset(EnumFacing enumFacing, int i);

    @Shadow
    public abstract BlockPos toImmutable();

    public Position rd$offset(Facing facing, int i) {
        return (Position) offset(EnumFacing.byName(facing.name()), i);
    }

    public Position rd$offset(Facing facing) {
        return (Position) offset(EnumFacing.byName(facing.name()));
    }

    public MutablePosition rd$toMutable() {
        return (MutablePosition) new BlockPos.MutableBlockPos((BlockPos) (Object) this);
    }

    public Position rd$toImmutable() {
        return (Position) toImmutable();
    }
}
