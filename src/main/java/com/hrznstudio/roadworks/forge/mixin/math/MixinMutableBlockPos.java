package com.hrznstudio.roadworks.forge.mixin.math;

import com.hrznstudio.roadworks.layer.api.common.math.MutablePosition;
import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.math.Vec3;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(BlockPos.MutableBlockPos.class)
@Implements(@Interface(iface = MutablePosition.class, prefix = "rd$"))
public abstract class MixinMutableBlockPos {
    @Shadow
    public abstract BlockPos.MutableBlockPos setPos(int i, int i1, int i2);

    public Position rd$set(int var1, int var2, int var3) {
        return (MutablePosition) setPos(var1, var2, var3);
    }

    public Position rd$set(Vec3 var1) {
        return rd$set((int) var1.getX(), (int) var1.getY(), (int) var1.getY());
    }

    public MutablePosition rd$toMutable() {
        return (MutablePosition) this;
    }
}