package com.hrznstudio.roadworks.forge.mixin.entity;

import com.hrznstudio.roadworks.layer.api.common.player.Profile;
import com.mojang.authlib.GameProfile;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

import java.util.UUID;

@Mixin(GameProfile.class)
@Implements(@Interface(iface = Profile.class, prefix = "rd$"))
public abstract class MixinGameProfile {
    @Shadow
    public abstract String getName();

    @Shadow
    public abstract UUID getId();

    public UUID rd$getUUID() {
        return getId();
    }

    public String rd$getUsername() {
        return getName();
    }
}