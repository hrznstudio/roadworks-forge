package com.hrznstudio.roadworks.forge.mixin;

import com.hrznstudio.roadworks.forge.util.Util;
import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;
import com.hrznstudio.roadworks.layer.api.common.world.WorldWriter;
import net.minecraft.block.state.IBlockState;
import net.minecraft.util.math.BlockPos;
import org.spongepowered.asm.mixin.Implements;
import org.spongepowered.asm.mixin.Interface;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(net.minecraft.world.IWorldWriter.class)
@Implements(@Interface(iface = WorldWriter.class, prefix = "rd$"))
public abstract class MixinWorldWriter {
    @Shadow
    public abstract boolean setBlockState(BlockPos blockPos, IBlockState iBlockState, int i);

    public void rd$setBlockState(Position pos, BlockState state) {
        setBlockState(Util.convert(pos), Util.convert(state), 3);
    }

    public void rd$setBlockState(Position pos, BlockState state, WorldWriter.Flags... flags) {
        int i = 0;
        for (WorldWriter.Flags flag : flags) {
            i |= flag.getId();
        }
        setBlockState(Util.convert(pos), Util.convert(state), i);
    }
}