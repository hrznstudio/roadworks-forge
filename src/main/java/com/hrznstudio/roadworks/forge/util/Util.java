package com.hrznstudio.roadworks.forge.util;

import com.hrznstudio.roadworks.forge.wrapper.TileEntityWrapper;
import com.hrznstudio.roadworks.forge.wrapper.TileEntityWrapperTickable;
import com.hrznstudio.roadworks.layer.api.common.block.Block;
import com.hrznstudio.roadworks.layer.api.common.block.BlockReader;
import com.hrznstudio.roadworks.layer.api.common.block.entity.BlockEntity;
import com.hrznstudio.roadworks.layer.api.common.entity.Entity;
import com.hrznstudio.roadworks.layer.api.common.fluid.Fluid;
import com.hrznstudio.roadworks.layer.api.common.math.Position;
import com.hrznstudio.roadworks.layer.api.common.player.Hand;
import com.hrznstudio.roadworks.layer.api.common.player.Player;
import com.hrznstudio.roadworks.layer.api.common.state.BlockState;
import com.hrznstudio.roadworks.layer.api.common.state.FluidState;
import com.hrznstudio.roadworks.layer.api.common.text.Text;
import com.hrznstudio.roadworks.layer.api.common.world.Facing;
import com.hrznstudio.roadworks.layer.api.common.world.World;
import com.hrznstudio.roadworks.layer.api.common.world.WorldReader;
import net.minecraft.block.state.IBlockState;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.fluid.IFluidState;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockRenderLayer;
import net.minecraft.util.EnumBlockRenderType;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.ITextComponent;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.world.IBlockReader;
import net.minecraft.world.IWorld;
import net.minecraft.world.IWorldReader;

public class Util {
    public static BlockPos convert(Position position) {
        if (position instanceof BlockPos) {
            return (BlockPos) position;
        }
        return new BlockPos(position.getX(), position.getY(), position.getZ());
    }

    public static Text convert(ITextComponent component) {
        return Text.of(component.getFormattedText()); //TODO: convert specific text types when available
    }

    public static ITextComponent convert(Text text) {
        return new TextComponentString(text.toString());
    }

    public static BlockState convert(IBlockState state) {
        return null;
    }
    public static IBlockState convert(BlockState state) {
        return null;
    }

    public static FluidState convert(IFluidState state) {
        return null;
    }

    public static Fluid convert(net.minecraft.fluid.Fluid fluid) {
        return (Fluid) fluid;
    }

    public static net.minecraft.fluid.Fluid convert(Fluid fluid) {
        if (fluid instanceof net.minecraft.fluid.Fluid)
            return (net.minecraft.fluid.Fluid) fluid;
        return null;
    }

    public static BlockReader convert(IBlockReader reader) {
        if (reader instanceof BlockReader)
            return (BlockReader) reader;
        return null;
    }

    public static WorldReader convert(IWorldReader reader) {
        if (reader instanceof WorldReader)
            return (WorldReader) reader;
        return null;
    }

    public static World convert(IWorld world) {
        if (world instanceof World)
            return (World) world;
        return null;
    }

    public static Player convert(EntityPlayer player) {
        return (Player) player;
    }

    public static Facing convert(EnumFacing facing) {
        switch (facing) {
            case UP:
                return Facing.UP;
            case DOWN:
                return Facing.DOWN;
            case EAST:
                return Facing.EAST;
            case WEST:
                return Facing.WEST;
            default:
            case NORTH:
                return Facing.NORTH;
            case SOUTH:
                return Facing.NORTH;
        }
    }

    public static World convert(net.minecraft.world.World world) {
        return (World) world;
    }

    public static Position convert(BlockPos pos) {
        return (Position) pos;
    }

    public static Hand convert(EnumHand hand) {
        return hand == EnumHand.MAIN_HAND ? Hand.PRIMARY : Hand.SECONDARY;
    }

    public static EnumBlockRenderType convert(Block.RenderType type) {
        switch (type) {
            default:
            case MODEL:
                return EnumBlockRenderType.MODEL;
            case ANIMATED:
                return EnumBlockRenderType.ENTITYBLOCK_ANIMATED;
            case INVISIBLE:
                return EnumBlockRenderType.INVISIBLE;
        }
    }

    public static Block.RenderLayer convert(BlockRenderLayer type) {
        switch (type) {
            default:
            case SOLID:
                return Block.RenderLayer.SOLID;
            case CUTOUT:
                return Block.RenderLayer.CUTOUT;
            case CUTOUT_MIPPED:
                return Block.RenderLayer.CUTOUT_MIPPED;
            case TRANSLUCENT:
                return Block.RenderLayer.TRANSLUCENT;
        }
    }

    public static Block convert(net.minecraft.block.Block block) {
        if (block instanceof Block)
            return (Block) block;
        return null; //TODO: Wrapper
    }

    public static Entity convert(net.minecraft.entity.Entity entity) {
        if (entity instanceof Entity)
            return (Entity) entity;
        return null; //TODO: Wrapper
    }

    public static TileEntity wrapper(boolean ticking, BlockEntity blockEntity) {
        if (ticking)
            return new TileEntityWrapperTickable(blockEntity);
        return new TileEntityWrapper(blockEntity);
    }
}