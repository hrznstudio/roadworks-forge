package com.hrznstudio.roadworks.forge;

import cpw.mods.modlauncher.api.IEnvironment;
import net.minecraftforge.fml.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static net.minecraftforge.fml.Logging.CORE;

public enum RoadworksPaths {
    GAMEDIR(),
    ROADWORKSDIR("roadworks"),
    CONFIGDIR("config", "roadworks"),
    PLUGINSDIR(ROADWORKSDIR, "plugins");

    private static final Logger LOGGER = LogManager.getLogger();
    private final Path relativePath;
    private final boolean isDirectory;
    private Path absolutePath;

    RoadworksPaths() {
        this("");
    }

    RoadworksPaths(String... path) {
        relativePath = computePath(path);
        this.isDirectory = true;
    }

    RoadworksPaths(boolean isDir, RoadworksPaths parent, String... path) {
        this.relativePath = parent.relativePath.resolve(computePath(path));
        this.isDirectory = isDir;
    }

    RoadworksPaths(RoadworksPaths parent, String... path) {
        this(true, parent, path);
    }

    public static void setup(IEnvironment env) {
        final Path rootPath = env.getProperty(IEnvironment.Keys.GAMEDIR.get()).orElseThrow(() -> new RuntimeException("No game path found"));
        loadAbsolutePaths(rootPath);
    }

    public static void loadAbsolutePaths(Path rootPath) {
        for (RoadworksPaths path : RoadworksPaths.values()) {
            path.absolutePath = rootPath.resolve(path.relativePath).toAbsolutePath().normalize();
            if (path.isDirectory) {
                FileUtils.getOrCreateDirectory(path.absolutePath, path.name());
            }
            LOGGER.debug(CORE, "Path {} is {}", () -> path, () -> path.absolutePath);
        }
    }

    private Path computePath(String... path) {
        return Paths.get(path[0], Arrays.copyOfRange(path, 1, path.length));
    }

    public Path get() {
        return absolutePath;
    }
}